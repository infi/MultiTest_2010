﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using DevComponents.DotNetBar;

namespace MultiTest_BLE
{
    public partial class FormMain : DevComponents.DotNetBar.Office2007RibbonForm  //Form
    {
        // 
        private FirmWareUpdate  frmFirmWareUpdate;
        private FormOption      frmOption;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            frmFirmWareUpdate   = new FirmWareUpdate();
            frmOption           = new FormOption();
        }

        private void btnOption_Click(object sender, EventArgs e)
        {
            if(frmOption.ShowDialog(this.Owner) == DialogResult.OK)
            {

            }
        }

        private void btnFirmWareUpdate_Click(object sender, EventArgs e)
        {
            frmFirmWareUpdate.Show();
        }

        private void btnStartTest_Click(object sender, EventArgs e)
        {

        }

        private void btnStopTest_Click(object sender, EventArgs e)
        {

        }

        private void btnMannualReport_Click(object sender, EventArgs e)
        {

        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {

        }

        private void btnMannualPicture_Click(object sender, EventArgs e)
        {

        }

        private void panelMain_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
