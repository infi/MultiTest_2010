﻿namespace MultiTest_BLE
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.ribbonControlMain = new DevComponents.DotNetBar.RibbonControl();
            this.labelItemName = new DevComponents.DotNetBar.LabelItem();
            this.styleManagerMain = new DevComponents.DotNetBar.StyleManager(this.components);
            this.barMianTool = new DevComponents.DotNetBar.Bar();
            this.imageListMain = new System.Windows.Forms.ImageList(this.components);
            this.btnMannualPicture = new DevComponents.DotNetBar.ButtonItem();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnViewReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnMannualReport = new DevComponents.DotNetBar.ButtonItem();
            this.labelItem2 = new DevComponents.DotNetBar.LabelItem();
            this.btnStartTest = new DevComponents.DotNetBar.ButtonItem();
            this.btnStopTest = new DevComponents.DotNetBar.ButtonItem();
            this.labelItem3 = new DevComponents.DotNetBar.LabelItem();
            this.btnFirmWareUpdate = new DevComponents.DotNetBar.ButtonItem();
            this.btnOption = new DevComponents.DotNetBar.ButtonItem();
            this.barStation = new DevComponents.DotNetBar.Bar();
            this.labelItemDevStatus = new DevComponents.DotNetBar.LabelItem();
            this.labelItem4 = new DevComponents.DotNetBar.LabelItem();
            this.labelItemTime = new DevComponents.DotNetBar.LabelItem();
            this.panelMain = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.barMianTool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barStation)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControlMain
            // 
            // 
            // 
            // 
            this.ribbonControlMain.BackgroundStyle.Class = "";
            this.ribbonControlMain.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControlMain.CaptionVisible = true;
            this.ribbonControlMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControlMain.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControlMain.Location = new System.Drawing.Point(5, 1);
            this.ribbonControlMain.Name = "ribbonControlMain";
            this.ribbonControlMain.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControlMain.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItemName});
            this.ribbonControlMain.Size = new System.Drawing.Size(891, 28);
            this.ribbonControlMain.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControlMain.TabGroupHeight = 14;
            this.ribbonControlMain.TabIndex = 0;
            this.ribbonControlMain.Text = "ribbonControlBLE";
            // 
            // labelItemName
            // 
            this.labelItemName.Image = ((System.Drawing.Image)(resources.GetObject("labelItemName.Image")));
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Text = "MultiTest_BLE";
            // 
            // styleManagerMain
            // 
            this.styleManagerMain.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Blue;
            // 
            // barMianTool
            // 
            this.barMianTool.AntiAlias = true;
            this.barMianTool.Dock = System.Windows.Forms.DockStyle.Top;
            this.barMianTool.Images = this.imageListMain;
            this.barMianTool.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnMannualPicture,
            this.labelItem1,
            this.btnViewReport,
            this.btnMannualReport,
            this.labelItem2,
            this.btnStartTest,
            this.btnStopTest,
            this.labelItem3,
            this.btnFirmWareUpdate,
            this.btnOption});
            this.barMianTool.Location = new System.Drawing.Point(5, 29);
            this.barMianTool.Name = "barMianTool";
            this.barMianTool.Size = new System.Drawing.Size(891, 59);
            this.barMianTool.Stretch = true;
            this.barMianTool.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barMianTool.TabIndex = 1;
            this.barMianTool.TabStop = false;
            this.barMianTool.Text = "bar1";
            // 
            // imageListMain
            // 
            this.imageListMain.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListMain.ImageStream")));
            this.imageListMain.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListMain.Images.SetKeyName(0, "Mainchart.png");
            this.imageListMain.Images.SetKeyName(1, "MainGetPic.png");
            this.imageListMain.Images.SetKeyName(2, "MainToolOption.png");
            this.imageListMain.Images.SetKeyName(3, "MainToolStart.png");
            this.imageListMain.Images.SetKeyName(4, "MainToolStop.png");
            this.imageListMain.Images.SetKeyName(5, "MainViewReport.png");
            this.imageListMain.Images.SetKeyName(6, "MainUpdate.png");
            // 
            // btnMannualPicture
            // 
            this.btnMannualPicture.ImageIndex = 1;
            this.btnMannualPicture.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMannualPicture.Name = "btnMannualPicture";
            this.btnMannualPicture.Text = "手动截图";
            this.btnMannualPicture.Click += new System.EventHandler(this.btnMannualPicture_Click);
            // 
            // labelItem1
            // 
            this.labelItem1.BorderSide = DevComponents.DotNetBar.eBorderSide.Left;
            this.labelItem1.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem1.Name = "labelItem1";
            // 
            // btnViewReport
            // 
            this.btnViewReport.ImageIndex = 5;
            this.btnViewReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Text = "查看报告";
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // btnMannualReport
            // 
            this.btnMannualReport.ImageIndex = 0;
            this.btnMannualReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMannualReport.Name = "btnMannualReport";
            this.btnMannualReport.Text = "手动生成报表";
            this.btnMannualReport.Click += new System.EventHandler(this.btnMannualReport_Click);
            // 
            // labelItem2
            // 
            this.labelItem2.BorderSide = DevComponents.DotNetBar.eBorderSide.Left;
            this.labelItem2.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem2.Name = "labelItem2";
            // 
            // btnStartTest
            // 
            this.btnStartTest.ImageIndex = 3;
            this.btnStartTest.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnStartTest.Name = "btnStartTest";
            this.btnStartTest.Text = "启动测试";
            this.btnStartTest.Click += new System.EventHandler(this.btnStartTest_Click);
            // 
            // btnStopTest
            // 
            this.btnStopTest.ImageIndex = 4;
            this.btnStopTest.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnStopTest.Name = "btnStopTest";
            this.btnStopTest.Text = "停止测试";
            this.btnStopTest.Click += new System.EventHandler(this.btnStopTest_Click);
            // 
            // labelItem3
            // 
            this.labelItem3.BorderSide = DevComponents.DotNetBar.eBorderSide.Left;
            this.labelItem3.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem3.Name = "labelItem3";
            // 
            // btnFirmWareUpdate
            // 
            this.btnFirmWareUpdate.ImageIndex = 6;
            this.btnFirmWareUpdate.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnFirmWareUpdate.Name = "btnFirmWareUpdate";
            this.btnFirmWareUpdate.Text = "升级固件";
            this.btnFirmWareUpdate.Click += new System.EventHandler(this.btnFirmWareUpdate_Click);
            // 
            // btnOption
            // 
            this.btnOption.ImageIndex = 2;
            this.btnOption.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOption.Name = "btnOption";
            this.btnOption.Text = "选项";
            this.btnOption.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // barStation
            // 
            this.barStation.AntiAlias = true;
            this.barStation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barStation.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItemDevStatus,
            this.labelItem4,
            this.labelItemTime});
            this.barStation.Location = new System.Drawing.Point(5, 607);
            this.barStation.Name = "barStation";
            this.barStation.Size = new System.Drawing.Size(891, 25);
            this.barStation.Stretch = true;
            this.barStation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barStation.TabIndex = 2;
            this.barStation.TabStop = false;
            this.barStation.Text = "bar1";
            // 
            // labelItemDevStatus
            // 
            this.labelItemDevStatus.Image = ((System.Drawing.Image)(resources.GetObject("labelItemDevStatus.Image")));
            this.labelItemDevStatus.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.labelItemDevStatus.Name = "labelItemDevStatus";
            this.labelItemDevStatus.Text = "设备已断开";
            // 
            // labelItem4
            // 
            this.labelItem4.BorderSide = DevComponents.DotNetBar.eBorderSide.Left;
            this.labelItem4.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem4.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.labelItem4.Name = "labelItem4";
            // 
            // labelItemTime
            // 
            this.labelItemTime.Image = ((System.Drawing.Image)(resources.GetObject("labelItemTime.Image")));
            this.labelItemTime.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.labelItemTime.Name = "labelItemTime";
            this.labelItemTime.Text = "2016-08-27 22:45:15";
            this.labelItemTime.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // panelMain
            // 
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(5, 88);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(891, 519);
            this.panelMain.TabIndex = 3;
            this.panelMain.Paint += new System.Windows.Forms.PaintEventHandler(this.panelMain_Paint);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 634);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.barStation);
            this.Controls.Add(this.barMianTool);
            this.Controls.Add(this.ribbonControlMain);
            this.Name = "FormMain";
            this.Text = "MultiTest_BLE";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barMianTool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barStation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl ribbonControlMain;
        private DevComponents.DotNetBar.LabelItem labelItemName;
        private DevComponents.DotNetBar.StyleManager styleManagerMain;
        private DevComponents.DotNetBar.Bar barMianTool;
        private DevComponents.DotNetBar.ButtonItem btnMannualPicture;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnViewReport;
        private DevComponents.DotNetBar.ButtonItem btnMannualReport;
        private DevComponents.DotNetBar.LabelItem labelItem2;
        private DevComponents.DotNetBar.ButtonItem btnStartTest;
        private DevComponents.DotNetBar.ButtonItem btnStopTest;
        private DevComponents.DotNetBar.LabelItem labelItem3;
        private DevComponents.DotNetBar.ButtonItem btnOption;
        private DevComponents.DotNetBar.ButtonItem btnFirmWareUpdate;
        private DevComponents.DotNetBar.Bar barStation;
        private DevComponents.DotNetBar.LabelItem labelItemDevStatus;
        private DevComponents.DotNetBar.LabelItem labelItem4;
        private DevComponents.DotNetBar.LabelItem labelItemTime;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.ImageList imageListMain;
    }
}

