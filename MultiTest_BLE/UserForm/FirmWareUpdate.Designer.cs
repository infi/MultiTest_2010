﻿namespace MultiTest_BLE
{
    partial class FirmWareUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FirmWareUpdate));
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.styleManagerFWUpdate = new DevComponents.DotNetBar.StyleManager(this.components);
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.barProgressStatus = new DevComponents.DotNetBar.Bar();
            this.panelFW = new System.Windows.Forms.Panel();
            this.btnFileSelect = new System.Windows.Forms.Button();
            this.tbFilePath = new System.Windows.Forms.TextBox();
            this.progressBarUpdate = new System.Windows.Forms.ProgressBar();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.barProgressStatus)).BeginInit();
            this.panelFW.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.Class = "";
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(5, 1);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControl1.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.ribbonControl1.Size = new System.Drawing.Size(638, 44);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 0;
            this.ribbonControl1.Text = "ribbonControl1";
            // 
            // styleManagerFWUpdate
            // 
            this.styleManagerFWUpdate.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Blue;
            // 
            // labelItem1
            // 
            this.labelItem1.Image = ((System.Drawing.Image)(resources.GetObject("labelItem1.Image")));
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "固件升级";
            // 
            // barProgressStatus
            // 
            this.barProgressStatus.AntiAlias = true;
            this.barProgressStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barProgressStatus.Location = new System.Drawing.Point(5, 314);
            this.barProgressStatus.Name = "barProgressStatus";
            this.barProgressStatus.Size = new System.Drawing.Size(638, 25);
            this.barProgressStatus.Stretch = true;
            this.barProgressStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barProgressStatus.TabIndex = 1;
            this.barProgressStatus.TabStop = false;
            this.barProgressStatus.Text = "bar1";
            // 
            // panelFW
            // 
            this.panelFW.Controls.Add(this.btnUpdate);
            this.panelFW.Controls.Add(this.progressBarUpdate);
            this.panelFW.Controls.Add(this.tbFilePath);
            this.panelFW.Controls.Add(this.btnFileSelect);
            this.panelFW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFW.Location = new System.Drawing.Point(5, 45);
            this.panelFW.Name = "panelFW";
            this.panelFW.Size = new System.Drawing.Size(638, 269);
            this.panelFW.TabIndex = 2;
            // 
            // btnFileSelect
            // 
            this.btnFileSelect.Location = new System.Drawing.Point(454, 54);
            this.btnFileSelect.Name = "btnFileSelect";
            this.btnFileSelect.Size = new System.Drawing.Size(75, 23);
            this.btnFileSelect.TabIndex = 0;
            this.btnFileSelect.Text = "选择固件";
            this.btnFileSelect.UseVisualStyleBackColor = true;
            this.btnFileSelect.Click += new System.EventHandler(this.btnFileSelect_Click);
            // 
            // tbFilePath
            // 
            this.tbFilePath.Location = new System.Drawing.Point(12, 54);
            this.tbFilePath.Name = "tbFilePath";
            this.tbFilePath.Size = new System.Drawing.Size(422, 21);
            this.tbFilePath.TabIndex = 1;
            // 
            // progressBarUpdate
            // 
            this.progressBarUpdate.Location = new System.Drawing.Point(12, 103);
            this.progressBarUpdate.Name = "progressBarUpdate";
            this.progressBarUpdate.Size = new System.Drawing.Size(422, 23);
            this.progressBarUpdate.TabIndex = 2;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(454, 103);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "确定";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // FirmWareUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 341);
            this.Controls.Add(this.panelFW);
            this.Controls.Add(this.barProgressStatus);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "FirmWareUpdate";
            this.Text = "FirmWareUpdate";
            this.Load += new System.EventHandler(this.FirmWareUpdate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barProgressStatus)).EndInit();
            this.panelFW.ResumeLayout(false);
            this.panelFW.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.StyleManager styleManagerFWUpdate;
        private DevComponents.DotNetBar.Bar barProgressStatus;
        private System.Windows.Forms.Panel panelFW;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ProgressBar progressBarUpdate;
        private System.Windows.Forms.TextBox tbFilePath;
        private System.Windows.Forms.Button btnFileSelect;
    }
}